package sai.sql;

import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;

public class Rsmd {

public static void main(String args []){
		
		try{
			
			Class.forName("org.postgresql.Driver");
			Connection conn =DriverManager.getConnection("jdbc:postgresql://localhost:5432/irs", "postgres","satya");
			
			PreparedStatement stmt=conn.prepareStatement("select * from users");  
			ResultSet rs=stmt.executeQuery();  
			 
			 
			ResultSetMetaData rsmd=rs.getMetaData();  
			System.out.println("Total columns: "+rsmd.getColumnCount());  
			System.out.println("Column Name of 1st column: "+rsmd.getColumnName(1));  
			System.out.println("Column Type Name of 1st column: "+rsmd.getColumnTypeName(1));  
			
			DatabaseMetaData dbmd=conn.getMetaData();  
			System.out.println("Driver Name: "+dbmd.getDriverName());  
			System.out.println("Driver Version: "+dbmd.getDriverVersion());  
			System.out.println("UserName: "+dbmd.getUserName());  
			System.out.println("Database Product Name: "+dbmd.getDatabaseProductName());  
			System.out.println("Database Product Version: "+dbmd.getDatabaseProductVersion());  
		
			String table[]={"TABLE"};  
			ResultSet rs1=dbmd.getTables(null,null,null,table);  
			  
			while(rs1.next()){  
			System.out.println(rs1.getString(3));  
			}  
			
		}catch(Exception e){
			
			System.out.println(e);
		}
}
}
