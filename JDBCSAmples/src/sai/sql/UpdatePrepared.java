package sai.sql;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;

public class UpdatePrepared {

	public static void main(String args []){
		
		try{
			Class.forName("org.postgresql.Driver");
			
			Connection conn = DriverManager.getConnection("jdbc:postgresql://localhost:5432/irs", "postgres","satya");
			
			PreparedStatement stmt=conn.prepareStatement("update users set password=? where id=?");  
			stmt.setString(1,"Sai");//1 specifies the first parameter in the query i.e. name  
			stmt.setInt(2,553);  
			  
			int i=stmt.executeUpdate();  
			System.out.println(i+" records updated");  
			
			
		}catch(Exception e){
			
			System.out.println(e);
		
		}
	}
	
}
