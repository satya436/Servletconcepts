package sai.sql;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.Properties;

public class PostgresqlCon {

	public static void main(String args[]) {

		try {
			// step1 load the driver class

			Class.forName("org.postgresql.Driver");

			// step2 create the connection object

			Connection conn = DriverManager.getConnection("jdbc:postgresql://localhost:5432/irs", "postgres", "satya");

			// step3 create the statement object
			
			Statement st = conn.createStatement();
			
			//step4 execute query  
			
			ResultSet rs = st.executeQuery("Select email_address from users;");
			
			System.out.println("Column 1 returned:");
			
			while (rs.next()) {
				
				System.out.println(rs.getString(1)+" ");
			}
			

			//step5 close the connection object  
			
			rs.close();
			st.close();
		} catch (Exception e) {
			System.out.println(e);
		}
	}

}
