package sai.sql;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;

public class DeletePrepared {

	public static void main(String args []){
		
		try{
			
			Class.forName("org.postgresql.Driver");
			Connection conn =DriverManager.getConnection("jdbc:postgresql://localhost:5432/irs", "postgres","satya");
			PreparedStatement stmt=conn.prepareStatement("delete from users where id=?");  
			stmt.setInt(1,553);  
			  
			int i=stmt.executeUpdate();  
			System.out.println(i+" records deleted");  
		}catch(Exception e){
			
			System.out.println(e);
		}
	}
}
