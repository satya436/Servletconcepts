package sai.sql;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

public class RetrievePrepared {

	public static void main(String args []){
		
		try{
			
			Class.forName("org.postgresql.Driver");
			Connection conn =DriverManager.getConnection("jdbc:postgresql://localhost:5432/irs", "postgres","satya");
			
			PreparedStatement stmt=conn.prepareStatement("select * from users");  
			ResultSet rs=stmt.executeQuery();  
			while(rs.next()){  
			System.out.println(rs.getInt(1)+" "+rs.getString(2));  
			}  
			
			
		}catch(Exception e){
			
			System.out.println(e);
		}
	}
}
