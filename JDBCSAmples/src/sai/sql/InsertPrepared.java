package sai.sql;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;

public class InsertPrepared {

	public static void main(String args[]) {

		try {

			Class.forName("org.postgresql.Driver");
			Connection conn= DriverManager.getConnection("jdbc:postgresql://localhost:5432/irs","postgres","satya");
			PreparedStatement ps = conn.prepareStatement("INSERT INTO public.users(id, email_address,password)VALUES (?, ?, ?);");
			ps.setInt(1, 554);
			ps.setString(2, "sai123@gmail.com");
			ps.setString(3, "sai123");
			
			int i= ps.executeUpdate();
			System.out.println(i+"record inserted");
			Statement st = conn.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,
				    ResultSet.CONCUR_READ_ONLY);
			
			ResultSet rs =st.executeQuery("select * from users;");
			
			rs.absolute(5);
			
			System.out.println(rs.getInt(1)+" "+rs.getString(2)+" "+rs.getString(3)+" "+rs.getString(4)+" "+rs.getString(5));  
			
	
			conn.close();
			
		} catch (Exception e) {
			System.out.println(e);
		}
	}
}
