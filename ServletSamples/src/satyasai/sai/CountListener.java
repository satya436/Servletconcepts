package satyasai.sai;

import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;

public class CountListener implements HttpSessionListener {
	
	int count;

	@Override
	public void sessionCreated(HttpSessionEvent arg0) {
		count++;
		System.out.println("No of sessions = "+count);
		
	}

	@Override
	public void sessionDestroyed(HttpSessionEvent arg0) {
		count--;
		System.out.println("No of sessions = "+count);
	}

}
