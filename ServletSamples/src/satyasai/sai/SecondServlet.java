package satyasai.sai;

import java.io.PrintWriter;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class SecondServlet extends HttpServlet {

	public void doPost(HttpServletRequest request,
			HttpServletResponse response) {
		try {

			response.setContentType("text/html");
			PrintWriter out = response.getWriter();

			// Listener to calculate no of session is closed
			HttpSession session = request.getSession();
			session.invalidate();
			out.print("session is closed");
			out.print("<br>");

			// get cookie name and value
			Cookie ck[] = request.getCookies();
			out.print("Cookie Name: " + ck[0].getName());
			out.print("<br>");
			out.print("Cookie Value: " + ck[0].getValue());
			out.close();

		} catch (Exception e) {
			System.out.println(e);
		}
	}
}
