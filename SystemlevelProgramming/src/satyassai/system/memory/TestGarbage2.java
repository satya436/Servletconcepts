package satyassai.system.memory;

public class TestGarbage2{  
	 public void finalize(){System.out.println("object is garbage collected");}  
	 public static void main(String args[]){  
		 Runtime r=Runtime.getRuntime();
	  TestGarbage2 s1=new TestGarbage2();  
	  TestGarbage2 s2=new TestGarbage2();  
	  s1=s2;  
	  
	  System.out.println("After creating 10000 instance, Free Memory: "+r.freeMemory()); 
	  System.gc();  
	  System.out.println("After gc(), Free Memory: "+r.freeMemory());
	 }  
	}  
